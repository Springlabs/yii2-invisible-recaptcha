<?php

/**
 * @link https://github.com/himiklab/yii2-recaptcha-widget
 * @copyright Copyright (c) 2014-2018 HimikLab
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace springdev\yii2\recaptcha;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Yii2 Google reCAPTCHA widget.
 *
 * For example:
 *
 * ```php
 * <?= $form->field($model, 'reCaptcha')->widget(
 *  ReCaptcha::className(),
 *  ['siteKey' => 'your siteKey']
 * ) ?>
 * ```
 *
 * or
 *
 * ```php
 * <?= ReCaptcha::widget([
 *  'name' => 'reCaptcha',
 *  'siteKey' => 'your siteKey',
 *  'widgetOptions' => ['class' => 'col-sm-offset-3']
 * ]) ?>
 * ```
 *
 * @see https://developers.google.com/recaptcha
 * @author HimikLab
 * @package himiklab\yii2\recaptcha
 */
class ReCaptcha extends InputWidget {

    const JS_API_URL = '//www.google.com/recaptcha/api.js';

    /** @var string Your sitekey. */
    public $siteKey;

    /** @var string Your callback. */
    public $callback = 'onSubmit';
    public $label = 'Submit';
    public $formId = '';
    public $options = [];
    public $widgetOptions = [];

    public function run() {
        $view = $this->view;
        if (empty($this->siteKey)) {
            throw new InvalidConfigException('Required `siteKey` param isn\'t set.');
        }

        $arguments = http_build_query([
            'hl' => $this->getLanguageSuffix(),
            'render' => 'explicit',
            'onload' => 'recaptchaOnloadCallback',
        ]);

        $view->registerJsFile(
                self::JS_API_URL, ['position' => $view::POS_END, 'async' => true, 'defer' => true], 'recaptcha-api'
        );
        $view->registerJs('
             function onSubmit(token) {
                    document.getElementById("' . $this->formId . '").submit();
            }
            '
                , $view::POS_END, 'recaptcha-onload');
        $this->customFieldPrepare();
        echo Html::submitButton($this->label, array_merge(
                        [
            'data-sitekey' => $this->siteKey,
            'data-callback' => $this->callback
                        ], $this->options));
    }

    protected function getLanguageSuffix() {
        $currentAppLanguage = Yii::$app->language;
        $langsExceptions = ['zh-CN', 'zh-TW', 'zh-TW'];

        if (strpos($currentAppLanguage, '-') === false) {
            return $currentAppLanguage;
        }

        if (in_array($currentAppLanguage, $langsExceptions)) {
            return $currentAppLanguage;
        }

        return substr($currentAppLanguage, 0, strpos($currentAppLanguage, '-'));
    }

    protected function getReCaptchaId() {
        if (isset($this->widgetOptions['id'])) {
            return $this->widgetOptions['id'];
        }

        if ($this->hasModel()) {
            return Html::getInputId($this->model, $this->attribute);
        }

        return $this->id . '-' . $this->name;
    }

    protected function customFieldPrepare() {
        $inputId = $this->getReCaptchaId();

        if ($this->hasModel()) {
            $inputName = Html::getInputName($this->model, $this->attribute);
        } else {
            $inputName = $this->name;
        }

        echo Html::input('hidden', $inputName, null, ['id' => $inputId]);
    }

}
